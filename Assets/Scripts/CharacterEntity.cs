﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public enum eWorldDirection
{
    left = -1,
    right = 1
}

public abstract class CharacterEntity : MovableEntity
{
    protected float _dirFactor;

    protected void ChangeDirection(float dirFactor)
    {
        if (dirFactor != _dirFactor)
        {
            Vector3 localScale = transform.localScale;
            if (localScale.x != dirFactor)
            {
                localScale.x = dirFactor;
                transform.localScale = localScale;

                if (moveCollider != null)
                {
                    Vector3 pos = transform.position;
                    pos.x += -localScale.x * moveCollider.offset.x * 2f;
                    transform.position = pos;
                }
            }
            _dirFactor = dirFactor;
        }
    }

    protected virtual eWorldDirection GetDirection() { return (eWorldDirection)_dirFactor; }

#if UNITY_EDITOR
    protected virtual void OnDrawGizmos()
    {
        if (Utils.showGizmos) return;

        Vector3 origin = (Vector3)moveCollider.offset;
        origin.x *= (float)GetDirection();
        origin = transform.position + origin;
        Vector3 size = moveCollider.size;

        Gizmos.color = new Color(0, 1, 0, .5f);
        Gizmos.DrawCube(origin, size);
    }
#endif


    protected override void OnLateUpdate() { }
    protected override void OnStart() { }
    protected override void OnUpdate() { }
    protected override void OnAwake() { }
}