﻿using UnityEngine;
using System.Collections;
using InControl;

public class EnemyEntity : CharacterEntity
{
    private Animator _animator;
    public Animator animator { get { return _animator; } }

    protected override void OnAwake()
    {
        base.OnAwake();
        _animator = GetComponent<Animator>();
    }
}
