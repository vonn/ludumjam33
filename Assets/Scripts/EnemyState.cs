﻿public interface iEnemyState
{
    void Enter(PlayableEntity playableEntity);
    iPlayableState HandleInput(PlayableEntity playableEntity, PlayableActions actions);
}