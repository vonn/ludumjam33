﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class MovableEntity : MonoBehaviour
{
    protected Collider2D[] _surroundingColliders;

    protected int _layerMask;
    protected Vector2 _vel;

    protected tk2dSprite _sprite;

    public float speed;
    public float upSpeedFactor; // depth feeling with up moves

    public string[] collideLayers = null;
    public BoxCollider2D moveCollider = null;

    private void Awake()
    {
        _sprite = GetComponent<tk2dSprite>();
    }

    private void Start()
    {
        _vel = new Vector2();
        _surroundingColliders = new Collider2D[16]; // HACK: hard-coded, but enough

        // clamp initial pos
        Warp(ClampPosition(transform.position));

        // Update layer mask for collisions
        _layerMask = 0;
        if (collideLayers != null)
        {
            for (int i = 0; i < collideLayers.Length; ++i)
            {
                _layerMask |= 1 << LayerMask.NameToLayer(collideLayers[i]);
            }
        }

        OnStart();
    }

    private void Update()
    {
        OnUpdate();
    }

    private void LateUpdate()
    {
        // clamp current vel
        Vector2 clampedMove = new Vector2((int)_vel.x, (int)_vel.y);
        // check if movement is needed (> 1px move)
        if (clampedMove.magnitude >= 1.0f)
        {
            _vel = _vel - clampedMove;

            // if movement has impact on surrounding colliders
            if (UpdateSurroundingColliders(clampedMove) > 0)
            {
                // check if complete move is possible
                if (!CanMove(clampedMove))
                {
                    // check each axis to adapt clamped movement
                    bool moveX = CanMoveX(clampedMove.x);
                    bool moveY = CanMoveY(clampedMove.y);

                    if (!moveX)
                    {
                        clampedMove.x = 0f;
                    }

                    if (!moveY)
                    {
                        clampedMove.y = 0f;
                    }
                }
            }

            if (clampedMove != Vector2.zero)
            {
                Warp(Translate(clampedMove));
            }
        }

        OnLateUpdate();
    }

    /// <summary>
    /// Updates the number of colliders impacted by movement (also updates _surroundingColliders list)
    /// </summary>
    private int UpdateSurroundingColliders(Vector2 movement)
    {
        if (moveCollider != null)
        {
            Vector2 moveCenter = moveCollider.offset;
            moveCenter.x *= transform.localScale.x;
            moveCenter += (Vector2)transform.position + movement;
            Vector2 moveMin = moveCenter - moveCollider.size / 2f;
            Vector2 moveMax = moveCenter + moveCollider.size / 2f;

            return Physics2D.OverlapAreaNonAlloc(moveMin, moveMax, _surroundingColliders, _layerMask);
        }

        return 0;
    }

    private bool CanMove(Vector2 movement)
    {
        if (moveCollider != null && _surroundingColliders != null)
        {
            Vector2 moveCenter = moveCollider.offset;
            moveCenter.x *= transform.localScale.x;
            moveCenter += (Vector2)transform.position + movement;
            Vector2 moveMin = moveCenter - moveCollider.size / 2f;
            Vector2 moveMax = moveCenter + moveCollider.size / 2f;

            for (int i = 0; i < _surroundingColliders.Length; ++i)
            {
                // Make sure we're not dealing with own collider
                if (_surroundingColliders[i] != null && _surroundingColliders[i].gameObject != gameObject)
                {
                    BoxCollider2D otherCol = (BoxCollider2D)_surroundingColliders[i];
                    Vector2 otherCenter = otherCol.offset;
                    /*Vector3 worldScale = transform.localScale;
                    Transform parent = transform.parent;
                    while (parent != null)
                    {
                        worldScale = Vector3.Scale(worldScale, parent.localScale);
                        parent = parent.parent;
                    }
                    otherCenter.x *= worldScale.x;*/
                    otherCenter += (Vector2)otherCol.transform.position;
                    Vector2 otherSize = otherCol.size;
                    Vector2 otherMin = otherCenter - otherSize / 2f;
                    Vector2 otherMax = otherCenter + otherSize / 2f;

                    // from vid
                    if ((moveMin.x < otherMax.x && moveMax.x > otherMin.x
                         && moveMin.y < otherMax.y && moveMax.y > otherMin.y) == true)
                    {
                        return false;
                    }

                    // check for damn overlap
                    if ((moveMin.x < otherMax.x || moveMax.x < otherMin.x) &&
                        (moveMin.y < otherMax.y || moveMax.y > otherMin.y))
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private bool CanMoveX(float x)
    {
        // TODO: take care of overhead
        return CanMove(new Vector2(x, 0));
    }

    private bool CanMoveY(float y)
    {
        // TODO: take care of overhead
        return CanMove(new Vector2(0, y));
    }

    private Vector2 Translate(Vector2 pos)
    {
        return pos + (Vector2)transform.position;
    }

    private Vector2 ClampPosition(Vector2 pos)
    {
        pos.x = (int)pos.x;
        pos.y = (int)pos.y;
        return pos;
    }

    private void Warp(Vector2 warpPos)
    {
        Vector3 pos = (Vector3)ClampPosition(warpPos);
        pos.z = pos.y;
        transform.position = pos;
    }

    public virtual void GoLeft(float factor = 1f)
    {
        _vel.x -= speed * Time.deltaTime * factor;
    }

    public virtual void GoRight(float factor = 1f)
    {
        _vel.x += speed * Time.deltaTime * factor;
    }

    public virtual void GoUp(float factor = 1f)
    {
        _vel.y += speed * Time.deltaTime * factor * upSpeedFactor;
    }

    public virtual void GoDown(float factor = 1f)
    {
        _vel.y -= speed * Time.deltaTime * factor * upSpeedFactor;
    }

    public virtual void Move(Vector2 moveFactor)
    {
        if (moveFactor.x > 0)
        {
            GoRight();
        }
        else if (moveFactor.x < 0)
        {
            GoLeft();
        }

        if (moveFactor.y > 0)
        {
            GoUp();
        }
        else if (moveFactor.y < 0)
        {
            GoDown();
        }
    }

    /* abstract */
    protected abstract void OnAwake();
    protected abstract void OnStart();
    protected abstract void OnUpdate();
    protected abstract void OnLateUpdate();
}
