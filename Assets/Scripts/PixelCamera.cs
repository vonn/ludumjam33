﻿using UnityEngine;
using System.Collections;

public class PixelCamera : MonoBehaviour
{
    private Camera cam;
    public int pixelsPerUnit = 1;
    public float scale = 2;

    void Start()
    {
        cam = GetComponent<Camera>();
        cam.orthographic = true;
        cam.orthographicSize = Screen.height / (float)pixelsPerUnit / 2f / scale;
    }
}
