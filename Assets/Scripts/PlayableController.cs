﻿using UnityEngine;
using System.Collections;
using InControl;

public abstract class PlayableController : MonoBehaviour
{
    private PlayableActions _playableActions;
    private iPlayableState _state;

    public PlayableEntity playableEntity;

    private void Start()
    {
        _playableActions = new PlayableActions();
        // TODO config file
        _playableActions.Left.AddDefaultBinding(Key.A);
        _playableActions.Right.AddDefaultBinding(Key.D);
        _playableActions.Up.AddDefaultBinding(Key.W);
        _playableActions.Down.AddDefaultBinding(Key.S);
        _playableActions.Attack.AddDefaultBinding(Key.E);

        _state = GetDefaultState();
        _state.Enter(playableEntity);
    }

    private void Update()
    {
        iPlayableState newState = _state.HandleInput(playableEntity, _playableActions);
        if (newState != null)
        {
            _state = newState;
            _state.Enter(playableEntity);
        }
    }

    protected abstract iPlayableState GetDefaultState();
}