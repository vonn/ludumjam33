﻿using UnityEngine;
using System.Collections;
using InControl;

public class PlayableEntity : CharacterEntity
{
    private PlayableActions _playableActions;
    private tk2dSpriteAnimator _animator;

    public tk2dSpriteAnimator animator { get { return _animator; } }

    public eWorldDirection startDirection;
    public BoxCollider2D attackCollider;

    protected override void OnStart()
    {
        base.OnStart();

        _animator = transform.FindChild("Sprite").GetComponent<tk2dSpriteAnimator>();

        ChangeDirection((float)startDirection);
    }

    public override void GoLeft(float factor = 1f)
    {
        base.GoLeft(factor);
        ChangeDirection((float)eWorldDirection.left);
    }

    public override void GoRight(float factor = 1f)
    {
        base.GoRight(factor);
        ChangeDirection((float)eWorldDirection.right);
    }

#if UNITY_EDITOR
    protected override void OnDrawGizmos()
    {
        if (!Utils.showGizmos) return;

        base.OnDrawGizmos();
        if (_sprite != null)
        {
            tk2dSpriteColliderDefinition[] spriteColliders = _sprite.CurrentSprite.customColliders;
            for (int i = 0; i < spriteColliders.Length; ++i)
            {
                var colliderDef = spriteColliders[i];
                if (colliderDef != null)
                {
                    switch (colliderDef.name)
                    {
                        case "AttackCollider":
                            Vector3 origin = colliderDef.origin;
                            origin.x *= (float)GetDirection();
                            origin = transform.position + origin;
                            origin.z -= 1;
                            Vector3 size = colliderDef.Size;

                            Gizmos.color = new Color(1, 1, 0, .5f);
                            Gizmos.DrawCube(origin, size);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
#endif
}
