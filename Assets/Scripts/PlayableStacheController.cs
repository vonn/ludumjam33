﻿using UnityEngine;
using System.Collections;

public class PlayableStacheController : PlayableController
{
    protected override iPlayableState GetDefaultState()
    {
        return new StacheIdleState();
    }
}