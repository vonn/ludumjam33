﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class StacheIdleState : iPlayableState
{
    public void Enter(PlayableEntity playableEntity)
    {
        if (playableEntity.animator != null)
        {
            playableEntity.animator.Play("Idle");
        }
    }

    public iPlayableState HandleInput(PlayableEntity playableEntity, PlayableActions actions)
    {
        if (actions.Move.IsPressed)
        {
            return new StacheMovingState();
        }

        if (actions.Attack.WasPressed)
        {
            return new StacheAttackingState();
        }

        return null;
    }
}

public class StacheMovingState : iPlayableState
{
    public void Enter(PlayableEntity playableEntity)
    {
        if (playableEntity.animator != null)
        {
            playableEntity.animator.Play("Run");
        }
    }

    public iPlayableState HandleInput(PlayableEntity playableEntity, PlayableActions actions)
    {
        if (!actions.Move.IsPressed)
        {
            return new StacheIdleState();
        }
        else if (actions.Attack.WasPressed)
        {
            return new StacheAttackingState();
        }
        else
        {
            // do move
            playableEntity.Move(actions.Move);
        }
        return null;
    }
}

public class StacheAttackingState : iPlayableState
{
    private float _lastAction;
    private bool _continueAnim;

    public void Enter(PlayableEntity playableEntity)
    {
        _lastAction = Time.time;    // TODO: time mgr
        _continueAnim = true;

        if (playableEntity.animator != null)
        {
            playableEntity.animator.Play("Attack");
            playableEntity.animator.AnimationEventTriggered = CheckAnimationEvent;
        }
    }

    public iPlayableState HandleInput(PlayableEntity playableEntity, PlayableActions actions)
    {
        if (!_continueAnim)
        {

            return new StacheIdleState();
        }

        if (actions.Attack.WasReleased)
        {
            _lastAction = Time.time;    // TODO: time mgr
        }

        BoxCollider2D attackCollider = playableEntity.attackCollider;
        if (attackCollider != null && attackCollider.enabled)
        {

        }

        return null;
    }

    private void CheckAnimationEvent(tk2dSpriteAnimator animator, tk2dSpriteAnimationClip clip, int frameNum)
    {
        tk2dSpriteAnimationFrame frame = clip.GetFrame(frameNum);

        if (frame.eventInfo == "AnimOver")
        {
            if (Time.time - _lastAction >= .3f) // TODO: time mgr // TODO: attack delay in config
            {
                _continueAnim = false;
            }
        }
    }
}