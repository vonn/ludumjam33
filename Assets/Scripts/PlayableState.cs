﻿public interface iPlayableState
{
    void Enter(PlayableEntity playableEntity);
    iPlayableState HandleInput(PlayableEntity playableEntity, PlayableActions actions);
}